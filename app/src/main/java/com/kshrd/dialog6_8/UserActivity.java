package com.kshrd.dialog6_8;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class UserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.setting_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.add_menu:

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                CharSequence[] users = {"Admin", "System", "Editor", "Poster", "QC", "Subscriber"};

                builder.setTitle("Choose user");

                builder.setItems(users, (dialog, which) -> {

                    /* Get layout container */
                    LinearLayout layoutContainer = findViewById(R.id.layout_container);

                    /* Create Card View */
                    CardView cardView = new CardView(this);;
                    cardView.setContentPadding(25,25,25,25);
                    cardView.setUseCompatPadding(true);
                    cardView.setCardElevation(4);
                    cardView.setRadius(15);
                    LinearLayout.LayoutParams cardLayout = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                    );
                    cardView.setLayoutParams(cardLayout);

                    /* Create Text View Username */
                    TextView textUsername = new TextView(this);
                    textUsername.setText(users[which]);
                    textUsername.setTextSize(18);
                    textUsername.setTypeface(Typeface.DEFAULT_BOLD);

                    CardView.LayoutParams usernameLayout = new CardView.LayoutParams(
                            CardView.LayoutParams.MATCH_PARENT,
                            CardView.LayoutParams.WRAP_CONTENT
                    );
                    usernameLayout.setMargins(20,20,20,20);
                    textUsername.setLayoutParams(usernameLayout);

                    /* Add Text View to Card View */
                    cardView.addView(textUsername);
                    /* Add Card View to Layout Container */
                    layoutContainer.addView(cardView);

                });

                builder.create().show();

                break;
            case R.id.remove_all_menu:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
