package com.kshrd.dialog6_8;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class RoleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_role);

        final Button btnApplyRole = findViewById(R.id.button_apply_role);

        btnApplyRole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(RoleActivity.this);

                CharSequence[] roles = {"Admin", "System", "Poster", "Author", "Editor", "Subscriber"};
                List<CharSequence> data = new ArrayList<>();

                builder.setTitle(R.string.title_role_dialog)
                        .setMultiChoiceItems(roles,
                                new boolean[] {true, true, false, false, false, false},
                                new DialogInterface.OnMultiChoiceClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                        if (isChecked) {
                                            data.add(roles[which]);
                                        } else {
                                            data.remove(roles[which]);
                                        }
                                    }
                                })
                        .setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final TextView textRole = findViewById(R.id.text_role);
                                String result = "";

                                for (CharSequence d : data) {
                                    result += d.toString() + " ";
                                }

                                textRole.setText(result);
                            }
                        });

                data.add(roles[0]);
                data.add(roles[1]);

                builder.create().show();

            }
        });


    }
}
