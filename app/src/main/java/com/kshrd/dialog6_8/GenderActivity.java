package com.kshrd.dialog6_8;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

public class GenderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gender);


        final Button btnGender = findViewById(R.id.button_gender);

        btnGender.setOnClickListener(v -> {

            AlertDialog.Builder builder = new AlertDialog.Builder(GenderActivity.this);

            String[] subjects = {"Android", "iOS", "Spring", "React Native"};

            // set header
            builder.setTitle(R.string.title_gender_dialog);
            builder.setIcon(R.drawable.ic_sentiment_very_satisfied_orange_32dp);

            // set body
            builder.setSingleChoiceItems(subjects, 0, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(GenderActivity.this,
                            subjects[which] + "",
                            Toast.LENGTH_LONG).show();
                }
            });

            // set footer
            builder.setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    int selectedIndex = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                    Object item = ((AlertDialog) dialog).getListView().getItemAtPosition(selectedIndex);
                    Toast.makeText(GenderActivity.this,
                            item + "",
                            Toast.LENGTH_LONG).show();
                }
            });

            builder.create();
            builder.show();

        });

    }
}
