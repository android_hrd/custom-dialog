package com.kshrd.dialog6_8.dialogcustom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.kshrd.dialog6_8.R;

public class CardDialog extends DialogFragment {

    private CardDialogListener listener;

    public interface CardDialogListener {
        void getMessage(String msg);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (CardDialogListener) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View view = LayoutInflater.from(getActivity()).inflate(
                R.layout.card_dialog_layout, null
        );

        final Button btnSend = view.findViewById(R.id.button_send);
        final EditText editMessage = view.findViewById(R.id.edit_message);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.getMessage(editMessage.getText().toString());
                dismiss();
            }
        });

        builder.setView(view);

        return builder.create();
    }
}
