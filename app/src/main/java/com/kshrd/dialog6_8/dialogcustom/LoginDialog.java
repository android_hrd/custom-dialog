package com.kshrd.dialog6_8.dialogcustom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.kshrd.dialog6_8.R;

public class LoginDialog extends DialogFragment {

    private LoginDialogListener listener;

    public interface LoginDialogListener {
        void getInput(String email, String password, boolean isRememberMe);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (LoginDialogListener) context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),
                R.style.CustomAlertDialog);

        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.login_dialog_layout, null);

        // set body
        builder.setView(view);

        final Button btnCancel = view.findViewById(R.id.button_cancel);
        final Button btnLogin = view.findViewById(R.id.button_login_dialog);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editEmail = view.findViewById(R.id.edit_email);
                EditText editPassword = view.findViewById(R.id.edit_password);
                CheckBox checkRememberMe = view.findViewById(R.id.checkBox);
                String email = editEmail.getText().toString();
                String password = editPassword.getText().toString();
                boolean isRememberMe = checkRememberMe.isChecked();
                listener.getInput(email, password, isRememberMe);
                dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return builder.create();
    }
}
