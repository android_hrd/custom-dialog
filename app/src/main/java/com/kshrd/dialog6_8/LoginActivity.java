package com.kshrd.dialog6_8;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.kshrd.dialog6_8.dialogcustom.CardDialog;
import com.kshrd.dialog6_8.dialogcustom.LoginDialog;

public class LoginActivity extends AppCompatActivity implements LoginDialog.LoginDialogListener, CardDialog.CardDialogListener {

    static final String LOGIN_DIALOG = "Login Dialog";
    static final String CARD_DIALOG = "Card Dialog";

    private TextView textMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        textMessage = findViewById(R.id.textMessage);

        final Button btnLogin = findViewById(R.id.button_login_dialog);
        final Button btnSaySth = findViewById(R.id.button_say_something);

        btnSaySth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CardDialog cardDialog = new CardDialog();
                cardDialog.show(getSupportFragmentManager(), CARD_DIALOG);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginDialog loginDialog = new LoginDialog();
                loginDialog.setCancelable(false);
                loginDialog.show(getSupportFragmentManager(), LOGIN_DIALOG);
            }
        });

    }

    @Override
    public void getInput(String email, String password, boolean isRememberMe) {
        Toast.makeText(this,
                email + "-" + password + "-" + isRememberMe,
                Toast.LENGTH_SHORT).show();

    }

    @Override
    public void getMessage(String msg) {
        textMessage.setText(msg);
    }
}
