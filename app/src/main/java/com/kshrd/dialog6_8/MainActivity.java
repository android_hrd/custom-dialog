package com.kshrd.dialog6_8;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button buttonShowDialog = findViewById(R.id.button_show_dialog);

        buttonShowDialog.setOnClickListener(v -> {

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            String[] subjects = {"Android", "iOS", "React Native", "Spring"};

            builder.setTitle("Please choose your subject:")
                    .setIcon(R.drawable.ic_sentiment_very_satisfied_orange_32dp);

            builder.setItems(subjects, (dialog, which) -> {
                        Toast.makeText(MainActivity.this,
                                "My subject is " + subjects[which],
                                Toast.LENGTH_SHORT).show();
                    });

//            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    Toast.makeText(MainActivity.this,
//                            "OK OK",
//                            Toast.LENGTH_SHORT).show();
//                }
//            });
//
//            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    Toast.makeText(MainActivity.this,
//                            "Opeation is cancelled",
//                            Toast.LENGTH_SHORT).show();
//                }
//            });
//
//            builder.setNeutralButton("Remind me latter", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    Toast.makeText(MainActivity.this,
//                            "See you again",
//                            Toast.LENGTH_SHORT).show();
//                }
//            });

            builder.create().show();

        });

    }
}
